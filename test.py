import unittest
from translator import *

class teest(unittest.TestCase):
	def test(self):
		self.assertEqual(morseTranslator('a b c d e f'), '.- / -... / -.-. / -.. / . / ..-. ')

def main():
    unittest.main()

if __name__ == '__main__':
    main()
